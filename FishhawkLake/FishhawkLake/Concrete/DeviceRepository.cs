﻿using FishhawkLake.Abstract;
using FishhawkLake.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Concrete
{
    public class DeviceRepository : Repository<Device>, IDeviceRepository
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        public DeviceRepository(FishhawkLakeDbContext context) : base(context)
        {

        }

        public FishhawkLakeDbContext FishhawkLakeDbContext
        {
            get { return Context as FishhawkLakeDbContext; }
        }

        public IEnumerable<Device> Devices
        {
            get { return db.Devices; }
        }
        
    }
}