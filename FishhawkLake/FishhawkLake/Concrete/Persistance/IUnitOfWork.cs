﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FishhawkLake.Abstract;

namespace FishhawkLake.Concrete.Persistance
{
    interface IUnitOfWork : IDisposable
    {
        IAlertRepository Alerts { get; }
        IAlertRepository Threshholds { get; }
        IDeviceRepository Devices { get; }
    }
}
