﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FishhawkLake.Abstract;
using FishhawkLake.Models;

namespace FishhawkLake.Concrete.Persistance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FishhawkLakeDbContext _context;

        public UnitOfWork(FishhawkLakeDbContext context)
        {
            _context = context;
            Alerts = new AlertRepository(_context);
            Threshholds = new AlertRepository(_context);
        }

        public IAlertRepository Alerts { get; private set; }

        public IAlertRepository Threshholds { get; private set; }

        public IDeviceRepository Devices { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}