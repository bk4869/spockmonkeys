﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;

namespace FishhawkLake.Controllers
{
    public class DataFromParticleController : Controller
    {
        private DatumFromParticleContext db = new DatumFromParticleContext();

        // GET: DataFromParticle
        public ActionResult Index()
        {
            return View(db.DataFromParticle.ToList());
        }

        // GET: DataFromParticle/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatumFromParticle datumFromParticle = db.DataFromParticle.Find(id);
            if (datumFromParticle == null)
            {
                return HttpNotFound();
            }
            return View(datumFromParticle);
        }

        // GET: DataFromParticle/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DataFromParticle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DeviceId,Time,Value")] DatumFromParticle datumFromParticle)
        {
            if (ModelState.IsValid)
            {
                db.DataFromParticle.Add(datumFromParticle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(datumFromParticle);
        }

        // GET: DataFromParticle/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatumFromParticle datumFromParticle = db.DataFromParticle.Find(id);
            if (datumFromParticle == null)
            {
                return HttpNotFound();
            }
            return View(datumFromParticle);
        }

        // POST: DataFromParticle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DeviceId,Time,Value")] DatumFromParticle datumFromParticle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(datumFromParticle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(datumFromParticle);
        }

        // GET: DataFromParticle/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatumFromParticle datumFromParticle = db.DataFromParticle.Find(id);
            if (datumFromParticle == null)
            {
                return HttpNotFound();
            }
            return View(datumFromParticle);
        }


    }
}
