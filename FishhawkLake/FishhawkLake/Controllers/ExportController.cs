﻿using FishhawkLake.Abstract;
using FishhawkLake.Concrete.Persistance;
using FishhawkLake.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FishhawkLake.Controllers
{
    public class ExportController : Controller
    {

        FishhawkLakeDbContext db = new FishhawkLakeDbContext();
        private IDeviceRepository repo;

        public ExportController() { }

        public ExportController(IDeviceRepository deviceRepo)
        {
            repo = deviceRepo;
        }

        /**
         * This method returns the View for the Export page.
         */
        [HttpGet]
        public ActionResult Export()
        {
            // create a list of all possible devices and add it to the ViewBag for the View to use in the dropdown menu.
            List<string> deviceIDs = new List<string>();
            deviceIDs.Add("All");
            deviceIDs.Add("Psi");
            deviceIDs.Add("Turbidity");
            deviceIDs.Add("Water Level");
            deviceIDs.Add("Chlorine");
            ViewBag.deviceIDs = deviceIDs;

            return View();
        }

        /**
         * This method creates a filtered list of data from the database and exports it to a .csv file.
         * @param collection the form data taken in from the View
         */
        [HttpPost]
        public ActionResult Export(FormCollection collection)
        {
            try
            {
                // get the StartDate and EndDate from the collection data and convert it into DateTime measurements.
                DateTime startDate = Convert.ToDateTime(collection["StartDate"]);
                DateTime endDate = Convert.ToDateTime(collection["EndDate"]);
                // create a list of all data in the devices table in descending order.
                IEnumerable<Device> list = db.Devices.OrderByDescending(x => x.Time).ToList();
                // get the DID from the collection data and translate that into a device ID recognized by the devices table.
                string deviceID = "pub" + collection["DID"].ToLower();
                
                // if "All" was selected in the dropdown menu, list equals the data from all devices in the date range given
                if(collection["Device"] == "All")
                {
                    list = db.Devices.OrderByDescending(x => x.Time)
                        .Where(x => x.Time >= startDate && x.Time <= endDate)
                        .ToList();
                }
                // otherwise list equals the data in the devices table where the DeviceID equals the given deviceID, and the data is in the
                // given date range.
                else
                {
                    list = db.Devices.OrderByDescending(x => x.Time)
                        .Where(x => x.DeviceID == deviceID)
                        .Where(x => x.Time >= startDate && x.Time <= endDate)
                        .ToList();
                }
                
                // using a StringBuilder object, create a string in the format Time, Value, DeviceID (one item per column) with titles, and
                // the data below that
                var sb = new StringBuilder();
                sb.AppendFormat("{0}, {1}, {2}, {3}", "Time", "Value", "Device ID", Environment.NewLine);
                foreach (var item in list)
                {
                    sb.AppendFormat("{0}, {1}, {2}, {3}", item.Time, item.Value, item.DeviceID, Environment.NewLine);
                }

                // create a .csv file, name it "FilteredAlerts.csv" and write the string we just created to it.
                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                response.AddHeader("content-disposition", "attachment;filename=FilteredAlerts.csv");
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();

                return RedirectToAction("Export");
            }
            // if something goes wrong, return the View.
            catch
            {
                return View();
            }

            
            
        }

        /**
         * This method creates list of all data from the database and exports it to a .csv file.
         */
        public void ExportAllCSV()
        {
            // create a list of all data in the database
            IEnumerable<Device> list = db.Devices.OrderBy(x => x.Time).ToList();

            // using a StringBuilder object, create a string in the format Time, Value, DeviceID (one item per column) with titles, and
            // the data below that
            var sb = new StringBuilder();
            sb.AppendFormat("{0}, {1}, {2}, {3}", "Time", "Value", "Device ID", Environment.NewLine);
            foreach(var item in list)
            {
                sb.AppendFormat("{0}, {1}, {2}, {3}", item.Time, item.Value, item.DeviceID, Environment.NewLine);
            }

            // create a .csv file, name it "Alerts.csv" and write the string we just created to it.
            var response = System.Web.HttpContext.Current.Response;
            response.BufferOutput = true;
            response.Clear();
            response.ClearHeaders();
            response.ContentEncoding = Encoding.Unicode;
            response.AddHeader("content-disposition", "attachment;filename=Alerts.csv");
            response.ContentType = "text/plain";
            response.Write(sb.ToString());
            response.End();
        }


    }
}