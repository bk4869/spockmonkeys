﻿using FishhawkLake.Class;
using FishhawkLake.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace FishhawkLake.Controllers.HelperClasses
{
    public static class WeatherControllerHelper
    {
        public static OpenWeatherMap GetDefaultWeatherMap()
        {
            OpenWeatherMap openWeatherMap = new OpenWeatherMap();
            openWeatherMap.cities = new Dictionary<string, string>();
            
            openWeatherMap.cities.Add("Fishhawk Lake", "4784205");
            
            return openWeatherMap;
        }
        public static ResponseWeather GetLongviewResponseWeather()
        {
            // api
            string api = "https://api.openweathermap.org/data/2.5/weather?q=" + "Longview" + "," + "us";
            string appid = "&appid=" + "257b1e4458cc5181a4eb41e1a38d67ed";
            string url = api + appid;

            HttpWebRequest apiRequest =
            WebRequest.Create(url) as HttpWebRequest;

            string apiResponse = "";
            using (HttpWebResponse response = apiRequest.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                apiResponse = reader.ReadToEnd();
            }
            /*End*/

            //fetch values from the JSON and add them to my Helper class
            ResponseWeather rootObject = JsonConvert.DeserializeObject<ResponseWeather>(apiResponse);

            return rootObject;

        }
        public static string GetWeatherList()
        {
            ResponseWeather rootObject = GetLongviewResponseWeather();

            StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr><th>Weather Description</th></tr>");
            sb.Append("<tr><td>City:</td><td>" +
            rootObject.name + "</td></tr>");
            sb.Append("<tr><td>Country:</td><td>" +
            rootObject.sys.country + "</td></tr>");
            sb.Append("<tr><td>Wind:</td><td>" +
            rootObject.wind.speed + " Km/h</td></tr>");
            sb.Append("<tr><td>Current Temperature:</td><td>" +
            (int)(rootObject.main.temp * 9 / 5 - 459.67) + " °F</td></tr>");
            sb.Append("<tr><td>Humidity:</td><td>" +
            rootObject.main.humidity + "</td></tr>");
            sb.Append("<tr><td>Weather:</td><td>" +
            rootObject.weather[0].description + "</td></tr>");
            sb.Append("</table>");
            return sb.ToString();
        }
        public static string GetWeatherGui()
        {
            ResponseWeather rootObject = GetLongviewResponseWeather();

            StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr><th>Weather Description</th></tr>");
            sb.Append("<tr><td>City:</td><td>" +
            rootObject.name + "</td></tr>");
            sb.Append("<tr><td>Country:</td><td>" +
            rootObject.sys.country + "</td></tr>");
            sb.Append("<tr><td>Wind:</td><td>" +
            rootObject.wind.speed + " Km/h</td></tr>");
            sb.Append("<tr><td>Current Temperature:</td><td>" +
            (int)(rootObject.main.temp * 9 / 5 - 459.67) + " °F</td></tr>");
            sb.Append("<tr><td>Humidity:</td><td>" +
            rootObject.main.humidity + "</td></tr>");
            sb.Append("<tr><td>Weather:</td><td>" +
            rootObject.weather[0].description + "</td></tr>");
            sb.Append("</table>");
            return sb.ToString();
        }


    }
}