﻿using FishhawkLake.Models;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using FishhawkLake.Class;
using System.Text;
using System.Data;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Web.UI;
using FishhawkLake.Controllers.HelperClasses;

namespace FishhawkLake.Controllers
{
    public class HomeController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public int Incrementer(int a)
        {
            int b = a + 1;
            return b;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        
        [HttpGet]
        public JsonResult DrawLineGraph(string devID, int dateRange = 1)
        {
            DateTime start = DateTime.Now.AddDays(-dateRange);
            var data = db.Devices.OrderByDescending(x => x.ID).
                Where(x => x.DeviceID == devID).
                Where(m => m.Time.Day >= start.Day).
                OrderBy(x => x.Time).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult LiveDataDisplay()
        {
            return View();
        }
        
        /**
         * This method is to return the View for the Dashboard.
         * Only a logged in user can see this page.
         */
        [Authorize]
        public ActionResult Dashboard()
        {
            ViewBag.Display = "none";

            return View();
        }
        
        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(ContactModel contact)
        {
            string email = contact.email;
            string subject = contact.subject;
            string message = contact.message;

            string title = subject + " From:" + email;
            AlertsController ac = new AlertsController();
            ac.MailHelper("fishhawklakewaterplant@gmail.com", title, message);

            ViewBag.result = "Email Send to Support Team";
            return View();
        }
        
    }
}