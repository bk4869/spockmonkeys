﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;

namespace FishhawkLake.Controllers
{
    /// <summary>
    /// Control Alert user info
    /// </summary>
    public class AlertUserContactsController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        // GET: AlertUserContacts
        public ActionResult Index()
        {
            return View(db.AlertUserContact.ToList());
        }

        // GET: AlertUserContacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlertUserContact alertUserContact = db.AlertUserContact.Find(id);
            if (alertUserContact == null)
            {
                return HttpNotFound();
            }
            return View(alertUserContact);
        }

        // GET: AlertUserContacts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AlertUserContacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AID,UID,PhoneTri,TextTri,Email,Select")] AlertUserContact alertUserContact)
        {
            if (ModelState.IsValid)
            {
                db.AlertUserContact.Add(alertUserContact);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(alertUserContact);
        }

        // GET: AlertUserContacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlertUserContact alertUserContact = db.AlertUserContact.Find(id);
            if (alertUserContact == null)
            {
                return HttpNotFound();
            }
            return View(alertUserContact);
        }

        // POST: AlertUserContacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AID,UID,PhoneTri,TextTri,Email,Select")] AlertUserContact alertUserContact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alertUserContact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(alertUserContact);
        }

        // GET: AlertUserContacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlertUserContact alertUserContact = db.AlertUserContact.Find(id);
            if (alertUserContact == null)
            {
                return HttpNotFound();
            }
            return View(alertUserContact);
        }

        // POST: AlertUserContacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AlertUserContact alertUserContact = db.AlertUserContact.Find(id);
            db.AlertUserContact.Remove(alertUserContact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
