using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using FishhawkLake.Models;
using FishhawkLake.Models.ViewModel;

using FishhawkLake;
using FishhawkLake.Controllers.HelperClasses;
using System.Diagnostics;
using FishhawkLake.Abstract;
using FishhawkLake.Concrete.Persistance;
using System.Web.Security;
using FishhawkLake.CustomFilters;


namespace FishhawkLake.Controllers
{
    /// <summary>
    /// Main Alert System COntroller
    /// </summary>
    [AuthLogAttribute(Roles = "Admin, Manager")]
    public class AlertsController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        private AlertsViewModel avm = new AlertsViewModel();
        
        private IAlertRepository repo;

        private UnitOfWork unitOfWork = new UnitOfWork(new FishhawkLakeDbContext());

        public AlertsController() { }

        public AlertsController(IAlertRepository alertRepo)
        {
            this.repo = alertRepo;
        }

        public ViewResult List(int page = 1)
        {
            AlertLogViewModel model = new AlertLogViewModel
            {
                Alerts = unitOfWork.Alerts.Alerts
                    .OrderBy(x => x.Time)
                    .Skip((page - 1) * 5)
                    .Take(5)
                    .ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = 5,
                    TotalItems = unitOfWork.Alerts.Alerts.Count()
                },
                DID = "All",
                StartDate = DateTime.Now.ToString("yyyy-MM-dd"),
                EndDate = DateTime.Now.ToString("yyyy-MM-dd")
            };
            return View(model);
        }

        //Default Alert Result
        private static Boolean textBool = false;
        private static Boolean emailBool = false;
        private static Boolean phoneBool = false;
        //Whether alerts are on/off
        private static Boolean alertBool = true;


        private static String textTri;
        private static String phoneTri;
        private static String ifttt_email = "trigger@applet.ifttt.com";
        private static String cust_email;
        private static String from_email = System.Web.Configuration.WebConfigurationManager.AppSettings["AlertEmailAccount"];

        //Get IFTTT Info
        public string getIFTTT()
        {
            return ifttt_email;
        }

        //Get FromEmail 
        public string getFromEmail()
        {
            return from_email;
        }

        // GET: Alerts
        public ActionResult Index()
        {
            return View();
        }
        


        //Assign Value to global variables
        private void assignData()
        {

            try {
                textTri = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.TextTri).First();
                
            }
            catch (Exception)
            {
                textTri = "None";
                
            }
            try {
                phoneTri = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.PhoneTri).First();
            }
            catch (Exception)
            {
                phoneTri = "None";
            }
            try {
                cust_email = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.Email).First();
            }
            catch (Exception)
            {
                cust_email = "None";
            }
            
        }

        public Boolean TextTest()
        {
            return textBool;
        }

        public Boolean PhoneTest()
        {
            return phoneBool;
        }

        public Boolean EmailTest()
        {
            return emailBool;
        }

        // Main alert method
        //Trigger when over threshold 
        public ActionResult AlertTrigger(String DID, double val)
        {
            ViewBag.trigger = "System is performed in safe range.";
            if (AlarmThresholdManager.HasPassedThreshold(DID,val))
            {
                String text = "Warning!! Device: " + DID + " is over the threshold. Current value: " + val + " Alarm Send!!!";
                String final = "No Alert Notification Select.";
                if (alertBool)
                {
                    if (emailBool)
                    {
                        SendEmail(text);
                        final = text;
                    }
                    if (textBool)
                    {
                        SendText(text);
                        final = text;
                    }
                    if (phoneBool)
                    {
                        SendPhone(text);
                        final = text;
                    }
                }

                addLog(DID, val);

                Debug.WriteLine("AlertTrigger:"+alertBool);

                ViewBag.trigger = final;

            }

            return View();
        }

        public void addLog(string DID, double val)
        {
            Alert newAlert = new Alert();
            newAlert.Time = DateTime.Now;
            newAlert.Value = val;
            newAlert.DeviceID = DID;

            db.Alerts.Add(newAlert);
            db.SaveChanges();
        }

        [HttpGet]
        public ActionResult AlertLog(int page = 1)
        {
            List<string> deviceIDs = new List<string>();
            deviceIDs.Add("All");
            deviceIDs.Add("Psi");
            deviceIDs.Add("Turbidity");
            deviceIDs.Add("Water Level");
            deviceIDs.Add("Chlorine");
            ViewBag.dids = deviceIDs;

            AlertLogViewModel model = new AlertLogViewModel
            {
                Alerts = db.Alerts
                    .OrderByDescending(x => x.Time)
                    .Skip((page - 1) * 25)
                    .Take(25)
                    .ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = 25,
                    TotalItems = db.Alerts.Count()
                },
                DID = "All",
                StartDate = DateTime.Now.ToString("yyyy-MM-dd"),
                EndDate = DateTime.Now.ToString("yyyy-MM-dd")
            };

            System.Diagnostics.Debug.WriteLine("device: " + model.DID);
            System.Diagnostics.Debug.WriteLine("startDate: " + model.StartDate);
            System.Diagnostics.Debug.WriteLine("endDate: " + model.EndDate);

            return View(model);
        }

        [HttpPost]
        public ActionResult AlertLog(FormCollection collection, int page = 1)
        {
            
            try
            {
                string startDate = collection["StartDate"];
                string endDate = collection["EndDate"];
                IEnumerable<Alert> list = db.Alerts.OrderByDescending(x => x.Time).ToList();
                string deviceID = "pub" + collection["DID"].ToLower();

                List<string> deviceIDs = new List<string>();
                deviceIDs.Add("All");
                deviceIDs.Add("Psi");
                deviceIDs.Add("Turbidity");
                deviceIDs.Add("Water Level");
                deviceIDs.Add("Chlorine");
                ViewBag.dids = deviceIDs;

                string did = collection["DID"].ToString();
                DateTime startTime = Convert.ToDateTime(startDate);
                DateTime endTime = Convert.ToDateTime(endDate);

                if (did == "All")
                {
                    list = db.Alerts.OrderByDescending(x => x.Time)
                        .Where(x => x.Time >= startTime && x.Time <= endTime)
                        .ToList();
                    
                }
                else
                {
                    list = db.Alerts.OrderByDescending(x => x.Time)
                        .Where(x => x.DeviceID == deviceID)
                        .Where(x => x.Time >= startTime && x.Time <= endTime)
                        .ToList();
                }

                AlertLogViewModel model = new AlertLogViewModel
                {
                    Alerts = list.Skip((page - 1) * 25).Take(25).OrderByDescending(x => x.Time).ToList(),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = 25,
                        TotalItems = list.Count(),
                        LastPage = (int)Math.Ceiling((decimal)list.Count() / 25)
                    },
                    DID = deviceID,
                    StartDate = startDate,
                    EndDate = endDate
                };

                System.Diagnostics.Debug.WriteLine("collection stuff: " + collection["DID"]);
                System.Diagnostics.Debug.WriteLine("device: " + deviceID);
                System.Diagnostics.Debug.WriteLine("startDate: " + startDate);
                System.Diagnostics.Debug.WriteLine("endDate: " + endDate);

                return View(model);
            }
            catch
            {
                return View();
            }
            
        }
        
        /// <summary>
        /// Method that actully send the email
        /// Using standard Gmail SMTp Client
        /// </summary>
        /// <param name="email">To Where</param>
        /// <param name="subject">What subject</param>
        /// <param name="message">What message</param>
        public void MailHelper(String email, String subject, String message)
        {
            SmtpClient mail = new SmtpClient("smtp.gmail.com");
            mail.Port = 587;
            mail.EnableSsl = true;
            string pw = System.Web.Configuration.WebConfigurationManager.AppSettings["AlertEmailPassword"];
            mail.Credentials = new NetworkCredential(from_email, pw);

            MailMessage mailmessage = new MailMessage();
            mailmessage.From = new MailAddress(from_email);
            mailmessage.To.Add(email);
            mailmessage.Subject = subject;
            mailmessage.Body = message;
            mail.Send(mailmessage);
        }

        //Send Text trigger to ifttt
        //text = message info
        private void SendText(String text)
        {
            MailHelper(ifttt_email, textTri, text);
        }

        //Send Email trigger to ifttt
        //text = message info
        private void SendEmail(String text)
        {
            MailHelper(cust_email, "!!!!System Warning!!!!", text);
        }

        //Send Phone Trigger to ifttt
        //text = message info
        private void SendPhone(String text)
        {
            MailHelper(ifttt_email, phoneTri, text);
        }

        //Dashboard Action Method
        [HttpGet]
        public ActionResult Dashboard()
        {
            assignData();

            ViewBag.fromEmail = from_email;
            ViewBag.toEmail = cust_email;
            ViewBag.phoneTrigger = phoneTri;
            ViewBag.textTrigger = textTri;



            if (!(emailBool || textBool || phoneBool))
            {
                ViewBag.checker = "No Alert Method Selected.";
            }

            ViewBag.psi = AlarmThresholdManager.RetureThreshold("pubpsi");
            ViewBag.trub = AlarmThresholdManager.RetureThreshold("pubturbidity");
            ViewBag.chlor = AlarmThresholdManager.RetureThreshold("pubchlorine");
            ViewBag.wl = AlarmThresholdManager.RetureThreshold("publevel");


            return View();
        }

        [HttpPost]
        public ActionResult Dashboard(AlertSelectionModel sele)
        {
            
            emailBool = sele.email;
            phoneBool = sele.phone;
            textBool = sele.text;
            alertBool = sele.AlertOn;

            ViewBag.TH = db.Threshold;

            ViewBag.fromEmail = from_email;
            ViewBag.toEmail = cust_email;
            ViewBag.phoneTrigger = phoneTri;
            ViewBag.textTrigger = textTri;

            ViewBag.psi = AlarmThresholdManager.RetureThreshold("pubpsi");
            ViewBag.trub = AlarmThresholdManager.RetureThreshold("pubturbidity");
            ViewBag.chlor = AlarmThresholdManager.RetureThreshold("pubchlorine");
            ViewBag.wl = AlarmThresholdManager.RetureThreshold("publevel");
            //Debug.WriteLine(sele.text);
            //Debug.WriteLine(textBool);



            ViewBag.checker = "Updated!";
            return View();
        }

        //Basic Alert Tester
        public String TestAlert()
        {
            AlertTrigger("pubPsi", 100);

            return "success";
        }


    }
}