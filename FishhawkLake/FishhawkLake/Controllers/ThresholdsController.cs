﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;
using FishhawkLake.Controllers.HelperClasses;
using System.Web.Security;
using FishhawkLake.CustomFilters;

namespace FishhawkLake.Controllers
{
    public class ThresholdsController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        private static int tmpID = 0;


        // GET: Thresholds
        public ActionResult Index()
        {
            return View(db.Threshold.ToList());
        }

        // GET: Thresholds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Threshold threshold = db.Threshold.Find(id);
            if (threshold == null)
            {
                return HttpNotFound();
            }
            return View(threshold);
        }

        // GET: Thresholds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Thresholds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DeviceID,Threshold1")] Threshold threshold)
        {
            if (ModelState.IsValid)
            {
                db.Threshold.Add(threshold);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(threshold);
        }

        // GET: Thresholds/Edit/5
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Threshold threshold = db.Threshold.Find(id);
            if (threshold == null)
            {
                return HttpNotFound();
            }
            return View(threshold);
        }

        // POST: Thresholds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult Edit([Bind(Include = "ID,DeviceID,Threshold1")] Threshold threshold)
        {
            if (ModelState.IsValid)
            {
                db.Entry(threshold).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(threshold);
        }

        // GET: Thresholds/Delete/5
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Threshold threshold = db.Threshold.Find(id);
            if (threshold == null)
            {
                return HttpNotFound();
            }
            return View(threshold);
        }

        // POST: Thresholds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            Threshold threshold = db.Threshold.Find(id);
            db.Threshold.Remove(threshold);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        ///Notification Trigger Detector
        /// </summary>
        /// <returns></returns>
        public JsonResult NotfTrigger()
        {
            //If the DB conection is close, open it.
            if(db.Database.Connection.State == ConnectionState.Closed)
            {
                db.Database.Connection.Open();
            }

            //Get the ID of the latest record in the DB
            int recID = db.Devices.OrderByDescending(p => p.Time).Select(p => p.ID).First();

            //If not over the threshold return 'cool'
            string result = "cool";
            //Get the DeviceID Name and Value by the ID privided previsouly.
            string deviceID = db.Devices.Where(p => p.ID == recID).Select(p => p.DeviceID).First();
            double value = db.Devices.Where(p => p.ID == recID).Select(p => p.Value).First();

                //Only check the record once
                if (recID != tmpID)
                {
                    //If over the threshold, combine Device ID and Value with '&&',
                    //The method will divided the string into two part by '&&'
                    //on the front-end.
                    if (AlarmThresholdManager.HasPassedThreshold(deviceID, value))
                    {
                        result = deviceID + "&&" + value;
                        tmpID = recID;
                    }
                }

            //string test = "testID&&TestContent";

            //Format as JSON to front-end
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
