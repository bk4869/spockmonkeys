﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using FishhawkLake.Abstract;
using FishhawkLake.Concrete.Persistance;
using System.Web.Security;
using FishhawkLake.CustomFilters;


namespace FishhawkLake.Controllers
{
    public class PostReqErrorsController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();


        //Is Data Valid
        public static Boolean IsDataValid(JObject dataPoint, FishhawkLakeDbContext db)
        {
            Boolean valid = false;

            try
            {
                String devName = (string)dataPoint["DeviceId"];
                Double value = double.Parse((string)dataPoint["Value"]);
                int devCount = db.Threshold.Where(p => p.DeviceID == devName).Count();
                Debug.WriteLine(value+" "+devName);
                if (devCount > 0 && value > 0) //True iff find same device name in DB and value > 0;
                {
                    valid = true;
                }
                Debug.WriteLine("IsDataValid: "+valid);

            }
            catch
            {
                return false;
            }

            return valid;
        }


        /**
         * Not Valid Data to DB
         * */
        public static void ErrorToDB(JObject dataPoint, string ip, FishhawkLakeDbContext db)
        {
            var error = db.PostReqError.Create();
            string jojo;
     
            if (dataPoint != null)
            {
                jojo = dataPoint.ToString();
            }
            else
            {
                jojo = "Empty";
            }
            error.DATA = jojo;
            error.IP = ip;
            error.Time = DateTime.Now;
            db.PostReqError.Add(error);
            db.SaveChanges();
            //Debug.WriteLine(jojo);
        }



        // GET: PostReqErrors
        public ActionResult Index()
        {
            return View(db.PostReqError.OrderByDescending(m => m.Time).ToList());
        }

        

        // GET: PostReqErrors/Delete/5
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostReqError postReqError = db.PostReqError.Find(id);
            if (postReqError == null)
            {
                return HttpNotFound();
            }
            return View(postReqError);
        }

        // POST: PostReqErrors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AuthLogAttribute(Roles = "Admin, Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            PostReqError postReqError = db.PostReqError.Find(id);
            db.PostReqError.Remove(postReqError);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //Get Loopback Address
        public string getLoopBack()
        {
            return "127.0.0.1";
        }

        /// <summary>
        /// IP Range detector
        /// Private IP only
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public string IPRange(string ip)
        {
            string[] div_ip = ip.Split('.');
            if (div_ip[0] == "10")
            {
                return "TypeA";
            }else if(div_ip[0]== "172")
            {
                return "TypeB";
            }else if (div_ip[0] == "192")
            {
                return "TypeC";
            }else
            {
                return "False";
            }

        }
    }
}
