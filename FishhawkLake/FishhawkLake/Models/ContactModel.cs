﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models
{
    public class ContactModel
    {
        //Contact person's email
        public string email { get; set; }
        //Contact person's subject
        public string subject { get; set; }
        //Contact person's message
        public string message { get; set; }
    
    }
}