﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models
{
    /*
     * Yes yes, the name is weird. But it was better 
     * than DataFromParticle and DatasfromParticle.
     * Because the plural isnt for particle (though 
     * it can be) the plural applies to the indevidual
     * datapoints. DataFromParticles is misleading. 
    */
    public class DatumFromParticle 
    {
        public int ID { get; set; }
        public string DeviceId { get; set; }
        public DateTime Time { get; set; } = DateTime.Now;
        public float Value { get; set; }// store as float

    }
}