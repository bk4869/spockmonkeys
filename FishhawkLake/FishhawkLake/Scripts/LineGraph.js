﻿window.onload = function () {
    // ajax call for PSI monitor for one day
    var source = "/Home/DrawLineGraph?devID=pubpsi&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });
            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "PSI", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for PSI monitor for seven days
    var source = "/Home/DrawLineGraph?devID=pubpsi&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });
            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "PSI7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for Water Level monitor for one day
    var source = "/Home/DrawLineGraph?devID=publevel&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "WaterLevel", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for Water Level monitor for seven days
    var source = "/Home/DrawLineGraph?devID=publevel&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "WaterLevel7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for Turbidity monitor for one day
    var source = "/Home/DrawLineGraph?devID=pubturbidity&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Turbidity", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for Turbidity monitor for seven days
    var source = "/Home/DrawLineGraph?devID=pubturbidity&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Turbidity7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for Chlorine monitor for one day
    var source = "/Home/DrawLineGraph?devID=pubchlorine&dateRange=1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Chlorine", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });

    // ajax call for Chlorine monitor for seven days
    var source = "/Home/DrawLineGraph?devID=pubchlorine&dateRange=7";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            var list = new Array();
            var str = data;
            $.each(str, function (i, point) {
                list[i] = str[i].Value;
            });

            var date1 = data[0].Time;
            var date2 = data[list.length - 1].Time;

            var dateRange = getDateRange(date1, date2);

            lineG(list, "Chlorine7", dateRange);
        },
        error: function () {
            alert("There was an error. Please try again.");
        }
    });


}

// function to get the date range used to calculate the amount of data shown on the graphs
function getDateRange(date1, date2) {
    // the date data passed in to the javascript comes in the form of Json data. We need to parse that and convert it into something 
    // the javascript can work with
    var start = parseInt(date1.replace(/\/Date\((\d+)\)\//g, "$1"));
    var current = parseInt(date2.replace(/\/Date\((\d+)\)\//g, "$1"));

    // once the dates have been parsed, they are in milliseconds. We need to then convert this into days
    // Therefore we subtract the start date from the current date to get the total number of days in milliseconds, divide it by 
    // 86400000, and round it off to the nearest integer.
    var test = Math.round((current - start) / 86400000);

    return test;
}

// the number of points in a line graph is dependent on the number of labels on the x-axis so we need to explicitly set how many labels
// there will be depending on the number of dates.
// It is dependent on the number of days of data because we want the graph to look even. We don't want a graph with only three points on
// it.
function getLabelList(list, dateRange) {
    // N will be the number of labels we have on the graph
    var N;
    // the line graph takes in an array of numbers for its label points, so we will need to create one based on N.
    var arr = new Array();
    // if we are showing one day's worth of data, we want one data point per hour in the day.
    if (dateRange == 1) {
        N = 24
    }
    // if we have between one and eight days, there would not be enough label points for the graph to see trends properly, so we 
    // multiply N by 3 to get more.
    if (dateRange > 1 && dateRange <= 8) {
        N = 3 * dateRange;
    }
    // if we have more than eight days, just have one label point per day.
    else {
        N = dateRange;
    }

    // now that we have how many label points we are going to have, fill the array with one number per data point.
    for (var i = 0; i < N; i++) {
        arr[i] = i;
    }

    return arr;
}

// each point on the line graph only displays one number. Therefore if we want to display more than the x most recent measurements, we 
// want to average the measurements from the date range we are going to display and divide them into the number of label points we will
// have.
function getAverages(list, arrLength) {
    var listLength = list.length;
    var chunkSize = Math.floor(listLength / arrLength);
    var sum = 0;
    var newList = new Array();
    var index = 0;
    for (var i = 0; i < arrLength; i++) {

        for (var j = 0; j < chunkSize && index < listLength; j++) {
            index = (chunkSize * i) + j;
            sum += list[index];
        }
        var tempAverage = sum / chunkSize;
        newList[i] = tempAverage;
        sum = 0;
    }
    return newList;
}

// function that draws the line graph
function lineG(list, label1, dateRange) {
    var arr = getLabelList(list, dateRange);
    var arrLength = arr.length;
    var averages = getAverages(list, arrLength);
    var config = {
        type: 'line',
        data: {
            labels: arr,
            datasets: [{
                label: label1 + ' Average',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: averages,
                fill: false,
            }]
        },
        options: {
            responsive: true,

            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },

        }
    };
    var ctx = document.getElementById(label1).getContext('2d')
    window.myLine = new Chart(ctx, config);
}

