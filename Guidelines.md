# Guidelines #

## Code ##

### C# ###
* Use standard C# naming conventions as shown [here](http://www.dofactory.com/reference/csharp-coding-standards)

### Javascript ###


## Style ##r

## Database ##
* Pluralize table names


## Git ##
* Use branches
	*On your own repository, be sure that you have your own feature branch
* Commit often (don't feel like you have to have made major, complete, changes or new features before committing)
* Write good commit messages- specificity is key
* Don't commit code that doesn't compile on your develop or master branch
	*Non-compiling or erroring commits are fine on your own personal feature branch- but specify that it is not working in the commit message. Specifying what you have tried is also useful. 
* It's Okay to have multiple testing files on your local repository, and on the feature branch of your remote repository, but do not merge these test files to the develop branch
* Resolve your own merge conflicts by first merging dev into your feature branch and testing thoroughly

## Pull Request Model ##

1. Before doing any coding, ensure you have pulled the latest changes from the upstream remote repository to your forked repository in both the master and dev branches. Push these changes to your remote repository.
2. Create your feature branches off of the dev branch and do all your development here. If you need to, create testing/ sub-feature branches off of your feature branches, and then merge them back into your feature branch. Commit often.
3. Once you are ready to merge your work into the overall project, __don't__. First do the following:
    1. Checkout the dev branch and pull any changes that have occurred since you branched.
	2. Checkout your feature branch and merge dev into your branch.
	3. Fix any merge conflicts.
	4. Test thoroughly by building and running the project, making sure everything still looks and works the way you intended.
4. Push your feature branch to your remote repository.
5. From your forked repository on [Bitbucket](https://bitbucket.org/), create a new [pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request):
    1. Ensure that the branch you are merging from is your feature branch on your forked repository.
	2. Ensure that the branch you are merging into is the __develop__ branch on the upstream repository (you will likely need to change this from master to develop).
	3. Fill out the title and description fields as necessary.
	4. If you are no longer going to be working on this feature branch, feel free to check the box to close the branch after the pull request is merged.
	5. Create the pull request.
6. If you continue to work on this feature branch before the pull request is accepted, or if the upstream repository suggests any changes before the merge, you can still make changes on your branch and push them to your remote repository. Doing so will automatically update your pull request.
7. Once your pull request is accepted and merged, pull the updates to your local dev branch and push to your remote repository.
