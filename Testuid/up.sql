CREATE TABLE[dbo].[FakeUsers]
(
    [ID] INT IDENTITY(1,1) NOT NULL,
    [Username] NVARCHAR(64) NOT NULL,
    [Email] NVARCHAR(64) NOT NULL,
    [Password] NVARCHAR(64) NOT NULL,
    CONSTRAINT [PK_dbo.FakeUsers] PRIMARY KEY CLUSTERED (ID ASC)
);

INSERT INTO [dbo].[FakeUsers](Username, Email, Password) VALUES
    ('Lucy', 'rli12@wou.edu', 'pwd00000'),
    ('Jenny', 'lucywou12@gmail.com', 'pwd11111'),
    ('Ken', 'lucylrn@hotmail.com', 'pwd22222')



CREATE TABLE[dbo].[Devices]
(
    [ID] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [DeviceID] INT FOREIGN KEY REFERENCES Devices(ID),
    [Timestamp] NVARCHAR(64) NOT NULL,
    [Human_readable_date] NVARCHAR(128) NOT NULL,
	--Make Value as INT
    [Value] NVARCHAR(64) NOT NULL,
    [Context] NVARCHAR(256) NOT NULL,
    [Type] NVARCHAR(64) NOT NULL,
    --CONSTRAINT [PK_dbo.Device1] PRIMARY KEY CLUSTERED (ID ASC),
    --CONSTRAINT [FK_dbo.Devices] FOREIGN KEY (DeviceID) REFERENCES dbo.Devices(ID)
);

GO